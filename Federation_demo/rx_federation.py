#! /usr/bin/python3
import pika
import sys

def callback(ch, method, properties, body):
    cid = "devVM"
    # print("[Consumer_id: {3}] Received {0}, properties: {1}, method: {2}".format(body, properties, method, cid))
    if type(properties.headers) is dict and "x-received-from" in properties.headers:
        print("[Consumer: {0}], Received message {1} in {3} from {2}.".format(cid, body, properties.headers["x-received-from"][0]["uri"], method.routing_key))
    else:
        print("[Consumer: {0}], Received message {1} in {2}.".format(cid, body, method.routing_key))
    ch.basic_ack(delivery_tag=method.delivery_tag)


# federation chaining
def federation_demo():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    # channel.exchange_declare(exchange="federation-ex", exchange_type="fanout")
    # channel.queue_declare(queue='federation', durable=True)
    # channel.queue_bind(queue='federation', exchange="federation-ex", routing_key="information")
    channel.basic_qos(prefetch_count=1)
    channel.basic_consume(queue='federation', on_message_callback=callback)
    channel.start_consuming()
    connection.close()

if __name__ == '__main__':
    print('[*] Waiting for messages. To exit press CTRL+C')
    try:
        federation_demo()
        
    except KeyboardInterrupt:
        print("中斷")
        sys.exit(0)
