rabbitmqctl set_parameter federation-upstream upstream-devVM '{"uri":"amqp://guest:guest@10.110.107.107:5672","expires":3600000}'
rabbitmqctl set_parameter federation-upstream-set upstream-dev '[{"upstream":"upstream-devVM"}]'
rabbitmqctl set_policy federation-policy '^federation$' '{"federation-upstream-set":"upstream-dev"}' --priority 1 --apply-to queues