import pika

severity = ["INFO", "WARNING", "ERROR"]


def demo():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='xx.xx.xx.xx', port=5672, virtual_host='/'))
    channel = connection.channel()
    channel.queue_declare(queue='task_queue', durable=True)
    channel.basic_publish(exchange='', routing_key='task_queue', properties=pika.BasicProperties(delivery_mode=2),
                          body="Hello World!")
    print(" [x] Sent 'Hello World!'")
    connection.close()

if __name__ == '__main__':
    demo()
    # test_exchanger()
