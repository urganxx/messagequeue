# Monitor Demonstration Guide 
## Start to Install
- rabbitmq (3.8.9)
- grafana (7.3.1)
- MySQL/mariaDB
- Redis
- python3
    - pika
    - peewee
    - MySQLdb 
    - glances (3.1.5)

### Perhaps System required
1. sudo yum install epel-release
2. sudo yum install platform-python-pip
3. sudo yum install gcc python3-devel
4. sudo pip install glances[all]
5. sudo yum install grafana
    1. choose add grafana repo method

## System Configuration 

### rabbitmq

```
rabbitmq-plugins enable rabbitmq_federation
rabbitmq-plugins enable rabbitmq_mqtt
rabbitmq-plugins enable rabbitmq_management
```
### grafana
```Bash   
sudo systemctl daemon-reload
sudo systemctl start grafana-server
sudo systemctl status grafana-server

sudo systemctl enable grafana-server
sudo /sbin/chkconfig --add grafana-server

./bin/grafana-server web
```

- Copies init.d script to /etc/init.d/grafana-server 
- Installs default file (environment vars) to /etc/sysconfig/grafana-server
- Copies configuration file to /etc/grafana/grafana.ini


### [glances] 

**Edit /etc/glances/glances.conf**

sample file is included in this repo

## Package Configuration

### [rabbitmq-MQTT]
```
mosquitto_pub -h localhost -t [EXCHANGE_NAME] -m "sent by mosquitto testing again"
```

**Edit rabbitmq.conf**
- mqtt.allow_anonymous = true
- mqtt.exchange = mqtt_ex
- mqtt.subscription_ttl = 86400000
- mqtt.prefetch = 10


### [grafana]

```MySQL
DROP DATABASE IF EXISTS grafana;
CREATE DATABASE grafana;
```
**Edit grafana.ini**

```
type = mysql
host = where/mysql/server/located/
name = schema_name
user = your/database/account
password = your/database/password
```

```
grafana-server --config=/etc/grafana/grafana.ini --homepath=/usr/share/grafana
systemctl restart grafana-server.service
```

### [mariadb]
1. sudo ./usr/bin/mysql_secure_installation
2. set up user and password


## Sample Website