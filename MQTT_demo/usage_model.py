import abc
from peewee import Model, CharField, IntegerField, DecimalField, DateTimeField, BooleanField, MySQLDatabase, SmallIntegerField, FloatField
from datetime import datetime


class BaseModel(Model):
    id = IntegerField(primary_key=True)
    hostname = CharField(help_text="device name")
    category = CharField(help_text="類別")
    timestamp = DateTimeField(formats="%Y%m%d %H:%M:%S", help_text="datainfo create time")

    class Meta:
        database = MySQLDatabase(host="10.21.107.201", user="root", passwd="Qwer@asdF", database="grafana")


class WorkloadCPU(BaseModel):

    class Meta:
        table_name = "workload_cpu"

    cpu_number = SmallIntegerField(help_text="CPU index")
    os = FloatField(help_text="usage percentage")
    idle = FloatField(help_text="usage percentage")
    user = FloatField(help_text="usage percentage")
    total = FloatField(help_text="usage percentage")


class WorkloadMEM(BaseModel):
    class Meta:
        table_name = "workload_mem"

    available = DecimalField(help_text="")
    used = DecimalField(help_text="")
    free = DecimalField(help_text="")
    inactive = DecimalField(help_text="")
    active = DecimalField(help_text="")
    shared = DecimalField(help_text="")
    buffers = DecimalField(help_text="")
    percent = FloatField(help_text="usage percentage")
    total = DecimalField(help_text="")


class TrafficIntf(BaseModel):
    class Meta:
        table_name = "traffic_intf"

    intf = CharField(help_text="interface name")
    time_since_update = DecimalField()
    cx = DecimalField()
    tx = DecimalField()
    rx = DecimalField()
    cumulative_cx = DecimalField()
    cumulative_tx = DecimalField()
    cumulative_rx = DecimalField()
    speed = FloatField(help_text="traffic rate")


class IOAccessRuntime(BaseModel):
    class Meta:
        table_name = "disk_access"

    device_name = CharField(help_text="Storage Disk Name")
    read_count = DecimalField(help_text="reading access number")
    read_bytes = DecimalField(help_text="reading bytes size")
    write_count = DecimalField(help_text="writing access number")
    write_bytes = DecimalField(help_text="writing bytes size")


class FileSystemInfo(BaseModel):
    class Meta:
        table_name = "system_space"

    partition_name = CharField(help_text="partition alias name")
    used = DecimalField(help_text="已使用大小")
    free = DecimalField(help_text="未使用大小")
    size = DecimalField(help_text="擁有空間大小")
    present = DecimalField(help_text="系統空間已使用比率")

