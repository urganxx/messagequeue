import pika
import json
from datetime import datetime

CRUD_OPTIONS = ["CREATE", "READ", "UPDATE", "DELETE"]
MQTT_EXCHANGE = "mqtt_ex"


def test_mqtt():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange=MQTT_EXCHANGE, exchange_type="topic")
    msg = dict()
    msg['max'] = 50
    msg['min'] = 20
    msg['timestamp'] = datetime.now().strftime("%Y%m%d %H:%M:%S")
    # print(msg)

    channel.basic_publish(exchange=MQTT_EXCHANGE, routing_key=CRUD_OPTIONS[1], body=json.dumps(msg))
    print(" [x] Sent sensor info into exchange")
    connection.close()


if __name__ == '__main__':
    test_mqtt()