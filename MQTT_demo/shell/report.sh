nohup glances -q -t 15 --export rabbitmq &
pid=$!
echo "kill -9 $pid" > /etc/glances/stop_report.sh
chmod 645 /etc/glances/stop_report.sh