import pika
import json
import re
from datetime import datetime
import peewee
from usage_model import WorkloadCPU, WorkloadMEM, TrafficIntf, FileSystemInfo, IOAccessRuntime
import time
import MySQLdb

CRUD_OPTIONS = ["CREATE", "READ", "UPDATE", "DELETE"]
MQTT_EXCHANGE = "mqtt_ex"
MODULE_OPTIONS = ["percpu", "load", "mem", "network", "diskio", "fs", "ip", "uptime", "docker"]


def cpu_items(arr: dict):
    cpu_idx = re.findall("^\d(1)$.cpu_number", json.dumps(arr.keys()))


def mqtt_callback(ch, method, properties, body):
    handler = "Topic {0} Handler".format(method.routing_key)
    print("[Consumer: {0}] Received message: {1}".format(handler, body))


def glances_callback(ch, method, properties, body):
    # handler = "Topic {0} Handler".format(method.routing_key)
    # print("[Consumer: {0}] Received message: {1}".format(handler, body))
    resp = dict()
    for item in re.split(', ', body.decode()):
        k, v = item.split('=')
        if k == "dateinfo":
            t = datetime.strptime(v, "%Y-%m-%dT%H:%M:%S.%f")
            resp[k] = t.strftime("%Y-%m-%d %H:%M:%S")
        else:
            resp[k] = v
    # print(json.dumps(resp))
    if resp['name'] == "percpu":
        # resp['category'] = resp['name']
        cpu_list = re.findall(r"(\d{1}\.cpu_number)", " ".join(resp.keys()))
        for idx in range(len(cpu_list)):
            # print(resp[f"{idx}.system"], resp[f"{idx}.guest"], resp[f"{idx}.idle"], resp[f"{idx}.user"],
            #       resp[f"{idx}.total"])
            js_str = {
                'timestamp': resp['dateinfo'],
                'hostname': resp['hostname'],
                'category': resp['name'],
                'cpu_number': idx,
                'os': resp[f"{idx}.system"],
                'idle': resp[f"{idx}.idle"],
                'user': resp[f"{idx}.user"],
                'total': resp[f"{idx}.total"]
            }
            # print(js_str)
            WorkloadCPU.insert(js_str).execute()
        # print("DB Insertion")
    elif resp['name'] == "mem":
        # print(resp['name'])
        # print(resp['available'],resp['used'],resp['cached'],resp['free'],resp['inactive'],resp['shared'],resp['buffers'],resp['total'],resp['percent'])
        js_str = {
            'timestamp': resp['dateinfo'],
            'hostname': resp['hostname'],
            'category': resp['name'],
            'available': int(resp['available'])/1024/1024,
            'used': int(resp['used'])/1024/1024,
            'free': int(resp['free'])/1024/1024,
            'inactive': int(resp['inactive'])/1024/1024,
            'active': int(resp['active'])/1024/1024,
            'shared': int(resp['shared'])/1024/1024,
            'buffers': int(resp['buffers'])/1024/1024,
            'total': int(resp['total'])/1024/1024,
            'percent': resp['percent']
        }
        WorkloadMEM.insert(js_str).execute()

    elif resp['name'] == "network":
        inc = re.findall(r"(\w[a-z0-9]{1,6}\.time_since_update)", " ".join(resp.keys()))
        for intf in inc:
            # print(resp[f"{intf_name}.cumulative_tx"], resp[f"{intf_name}.cumulative_rx"], resp[f"{intf_name}.cumulative_cx"])
            intf_name, item = intf.split(".")
            js_str = {
                'timestamp': resp['dateinfo'],
                'hostname': resp['hostname'],
                'category': resp['name'],
                'intf': intf_name,
                'time_since_update': resp[intf],
                'cx': int(resp[f"{intf_name}.cx"])/1024/1024,
                'tx': int(resp[f"{intf_name}.tx"])/1024/1024,
                'rx': int(resp[f"{intf_name}.rx"])/1024/1024,
                'cumulative_cx': int(resp[f"{intf_name}.cumulative_cx"])/1024/1024,
                'cumulative_tx': int(resp[f"{intf_name}.cumulative_tx"])/1024/1024,
                'cumulative_rx': int(resp[f"{intf_name}.cumulative_rx"])/1024/1024,
                'speed': resp[f"{intf_name}.speed"],
            }
            TrafficIntf.insert(js_str).execute()

    elif resp['name'] == "fs":
        # print(resp['name'])
        partitions = re.findall(r"(\s[^/]{0}\S[a-z]{0,5}\.fs_warning)", " ".join(resp.keys()))
        for pti in partitions:
            part_zone, f = str.lstrip(pti).split(".")
            # print(resp[f"{part_zone}.used"], resp[f"{part_zone}.free"], resp[f"{part_zone}.size"],
            # resp[f"{part_zone}.percent"])
            js_str = {
                'timestamp': resp['dateinfo'],
                'hostname': resp['hostname'],
                'category': resp['name'],
                'partition_name': part_zone,
                'used': int(resp[f"{part_zone}.used"])/1024/1024,
                'free': int(resp[f"{part_zone}.free"])/1024/1024,
                'size': int(resp[f"{part_zone}.size"])/1024/1024,
                'present': resp[f"{part_zone}.percent"]
            }
            FileSystemInfo.insert(js_str).execute()

    elif resp['name'] == "load":
        # print(resp['name'])
        # print(resp['cpucore'], resp['min1'], resp['min5'], resp['min15'])
        pass
    
    ch.basic_ack(delivery_tag=method.delivery_tag)


def mqtt_device():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    # channel.exchange_declare(exchange=MQTT_EXCHANGE, exchange_type="topic")
    mqtt_queue = channel.queue_declare(queue="mqtt", durable=True)
    for option in CRUD_OPTIONS:
        channel.basic_qos(prefetch_count=1)
        channel.queue_bind(queue=mqtt_queue.method.queue, exchange=MQTT_EXCHANGE, routing_key=option)
        channel.basic_consume(queue=mqtt_queue.method.queue, on_message_callback=mqtt_callback, auto_ack=True)

    channel.start_consuming()
    print(" [x] Sent sensor info into exchange")
    # connection.close()


def glances_retrieve():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=600, blocked_connection_timeout=300))
    channel = connection.channel()
    glances_queue = channel.queue_declare(queue="glances_queue", durable=True)
    channel.basic_qos(prefetch_count=1)
    channel.queue_bind(queue=glances_queue.method.queue, exchange="amq.rabbitmq.trace", routing_key="server_status")
    channel.basic_consume(queue=glances_queue.method.queue, on_message_callback=glances_callback)
    try:
        channel.start_consuming()
    except pika.connection.exceptions.StreamLostError:
        connection.close()
        time.sleep(5)
        glances_retrieve()
    
    print(" [x] Sent sensor info into exchange")
    # connection.close()


def safe_callback():
    print(f'this is safe_callback msg')
    # ch.basic_ack(delivery_tag=method.delivery_tag)


def pause_callback(ch, method, properties, body):
    print(f'this is pause_callback msg: tell publish to pause')
    ch.basic_ack(delivery_tag=method.delivery_tag)


def recovery_callback(ch, method, properties, body):
    print(f'this is recovery_callback msg: tell publish to start publish')
    ch.basic_ack(delivery_tag=method.delivery_tag)


def newRetrieve():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', heartbeat=180, blocked_connection_timeout=120))
    channel = connection.channel()
    connection.add_callback_threadsafe(callback=safe_callback)
    connection.add_on_connection_blocked_callback(callback=pause_callback)
    connection.add_on_connection_unblocked_callback(callback=recovery_callback)
    # channel.queue_bind(queue="glances_queue", exchange="amq.rabbitmq.trace", routing_key="server_status", no_ack=True)
    glances_queue = channel.queue_declare(queue="glances_queue", durable=True)
    channel.basic_consume(queue=glances_queue.method.queue, on_message_callback=glances_callback, auto_ack=False)
    channel.start_consuming()
    

if __name__ == '__main__':
    # mqtt_device()
    # db = peewee.MySQLDatabase(host="10.21.107.201", user="root", passwd="Qwer@asdF", database="grafana")
    # db.create_tables([WorkloadCPU, WorkloadMEM, TrafficIntf, FileSystemInfo, IOAccessRuntime])
    # glances_retrieve()
    newRetrieve()
    # db = MySQLdb.connect(host="10.21.107.201", user="root", passwd="Qwer@asdF", db="grafana")
    # cursor = db.cursor(MySQLdb.cursors.DictCursor)
    # cursor.execute("SELECT * FROM grafana.data_source;")
    # resp = cursor.fetchall()
    # [print(x['name']) for x in resp]
    # db.commit()
