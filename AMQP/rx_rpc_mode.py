#! /usr/bin/python3
import pika
import sys

class RpcServerDemo:

    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        self.channel = self.connection.channel()
        self.channel.queue_declare(queue="rpc_queue")
        self.channel.basic_qos(prefetch_count=100)

    @staticmethod
    def on_request(ch, method, props, body):
        ch.basic_publish(exchange='', routing_key=props.reply_to, properties=pika.BasicProperties(correlation_id=props.correlation_id), body=body)
        print("get request info: {0}".format(body))
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def run(self):
        self.channel.basic_consume(queue="rpc_queue", on_message_callback=self.on_request)
        self.channel.start_consuming()

    def close(self):
        self.connection.close()


if __name__ == '__main__':
    print('[*] Waiting for messages. To exit press CTRL+C')
    rpc = RpcServerDemo()
    try:
        rpc.run()
    except KeyboardInterrupt:
        print("中斷")
        rpc.close()
        sys.exit(0)