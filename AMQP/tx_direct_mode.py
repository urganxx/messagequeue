import pika

severity = ["INFO", "WARNING", "ERROR"]

def test_exchange_logs():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange="testLogs", exchange_type="direct")

    for level in severity:
        channel.basic_publish(exchange="testLogs", routing_key=level, body="this information will be saved into logs_queue")

    print(" [x] Sent testing logs into exchange")
    connection.close()

if __name__ == '__main__':
    test_exchange_logs()