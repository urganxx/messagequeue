import pika

def test_message_by_topic():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    rand_levels = ["pro.testing.log", "dev.stage", "implement.dev"]
    channel.exchange_declare(exchange="topicsEmit", exchange_type="topic")

    for level in rand_levels:
        channel.basic_publish(exchange="topicsEmit", routing_key=level, body=f"this information will be saved into {level} queue")

    print(" [x] Sent testing logs into exchange")
    connection.close()

if __name__ == '__main__':
    test_message_by_topic()