#! /usr/bin/python3
import pika
import sys

topics = ["pro.#", "*.stage", "*.dev"]

def topic_callback(ch, method, properties, body):
    handler = "Topic {0} Handler".format(method.routing_key)
    print("[Consumer: {0}] Received message: {1}".format(handler, body))
    
def topic_receiver():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange="topicsEmit", exchange_type="topic")
    amp_queue = channel.queue_declare(queue="federation")
    quene_in_memory = amp_queue.method.queue
    for topic in topics:
        channel.queue_bind(queue=quene_in_memory, exchange="topicsEmit", routing_key=topic)
        channel.basic_consume(queue=quene_in_memory, on_message_callback=topic_callback, auto_ack=True)

    channel.start_consuming()
    connection.close()

if __name__ == '__main__':
    print('[*] Waiting for messages. To exit press CTRL+C')
    
    try:
        topic_receiver()
    except KeyboardInterrupt:
        print("中斷")
        sys.exit(0)
