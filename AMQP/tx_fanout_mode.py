import pika

def test_exchanger():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange="demo1", exchange_type="fanout")
    channel.basic_publish(exchange="demo1", routing_key="", body="this message will broadcast to each queue 1.")
    print(" [x] Sent demo broadcast into exchange")
    connection.close()
    
if __name__ == '__main__':
    test_exchanger()