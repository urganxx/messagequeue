#! /usr/bin/python3
import pika
import sys

def callback(ch, method, properties, body):
    cid = "devVM"
    # print("[Consumer_id: {3}] Received {0}, properties: {1}, method: {2}".format(body, properties, method, cid))
    if type(properties.headers) is dict and "x-received-from" in properties.headers:
        print("[Consumer: {0}], Received message {1} in {3} from {2}.".format(cid, body, properties.headers["x-received-from"][0]["uri"], method.routing_key))
    else:
        print("[Consumer: {0}], Received message {1} in {2}.".format(cid, body, method.routing_key))
    ch.basic_ack(delivery_tag=method.delivery_tag)
    

def fans_receiver():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange="demo1", exchange_type="fanout")
    result = channel.queue_declare(queue="", exclusive=True)
    quene_in_memory = result.method.queue
    channel.queue_bind(queue=quene_in_memory, exchange="demo1", routing_key="")
    channel.basic_consume(queue=quene_in_memory, on_message_callback=callback)
    channel.start_consuming()
    

if __name__ == '__main__':
    print('[*] Waiting for messages. To exit press CTRL+C')
    try:
        fans_receiver()
    except KeyboardInterrupt:
        print("中斷")
        sys.exit(0)