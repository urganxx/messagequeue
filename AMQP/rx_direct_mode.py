#! /usr/bin/python3
import pika
import sys

severity = ["INFO", "WARNING", "ERROR"]

def log_callback(ch, method, properties, body):
    cid = "Log Handler"
    # print("[Consumer_id: {3}] Received {0}, properties: {1}, method: {2}".format(body, properties, method, cid))
    print("[Consumer: {0}] From {1} Received {2}".format(cid, method.routing_key, body))


def logs_receiver():
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()
    channel.exchange_declare(exchange="testLogs", exchange_type="direct")
    amp_queue = channel.queue_declare(queue="", exclusive=True)
    quene_in_memory = amp_queue.method.queue

    for level in severity:
        channel.queue_bind(queue=quene_in_memory, exchange="testLogs", routing_key=level)
        channel.basic_consume(queue=quene_in_memory, on_message_callback=log_callback, auto_ack=True)

    channel.start_consuming()
    connection.close()


if __name__ == '__main__':
    print('[*] Waiting for messages. To exit press CTRL+C')
    try:
        logs_receiver()
    except KeyboardInterrupt:
        print("中斷")
        sys.exit(0)