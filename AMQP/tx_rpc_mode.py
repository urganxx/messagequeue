import pika
import uuid

severity = ["INFO", "WARNING", "ERROR"]

class RemoteAgent:
    response = ''
    corr_id = ''

    def __init__(self):
        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        self.cached_queue = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = self.cached_queue.method.queue
        self.channel.basic_consume(queue=self.callback_queue, on_message_callback=self.on_resp_callback, auto_ack=True)

    def on_resp_callback(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, inp):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(exchange='', routing_key='rpc_queue', properties=pika.BasicProperties(reply_to=self.callback_queue,
                                   correlation_id=self.corr_id), body=inp)
        while self.response is None:
            self.connection.process_data_events(time_limit=10)

        return str(self.response)


if __name__ == '__main__':
    client_agent = RemoteAgent()
    real_resp = client_agent.call(inp="Demo_RPC_Proxy_Method")
    print(real_resp)