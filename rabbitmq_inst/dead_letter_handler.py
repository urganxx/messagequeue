#! /usr/bin/python3
import sys
import pika
from datetime import datetime
from m_libs import get_env_vars


def reject_cb(ch, method, properties, body):
	print("tag: {0} \t routing_key: {1} \t body: {2}".format(method.delivery_tag, method.routing_key, body))
	ch.basic_reject(delivery_tag=method.delivery_tag)


def accept_cb(ch, method, properties, body):
	print("tag: {0} \t routing_ley: {1} \t body: {2}".format(method.delivery_tag, method.routing_key, body.decode()))
	# do re-sent
	ch.basic_publish(exchange="", routing_key=method.routing_key, body=body.decode())
	ch.basic_ack(delivery_tag=method.delivery_tag)

	
def negative_cb(ch, method, properties, body):
	print("tag: {0} \t routing_ley: {1} \t body: {2}".format(method.delivery_tag, method.routing_key, body))
	ch.basic_nack(delivery_tag=method.delivery_tag)
	

if __name__ == "__main__":
	conf = get_env_vars()
	params = pika.ConnectionParameters("localhost")
	connection = pika.BlockingConnection(parameters=params)
	channel = connection.channel()
	
	# dlq = channel.queue_declare(queue=conf['Q']["DLQ_QUEUE"], durable=True)
	channel.basic_consume(queue=conf['Q']["DLQ_QUEUE"], on_message_callback=accept_cb, auto_ack=False)
	try:
		channel.start_consuming()
	except KeyboardInterrupt:
		print("中斷")
		sys.exit(0)