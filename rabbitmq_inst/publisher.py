import pika
import json
import uuid
from datetime import datetime
import time
from m_libs import get_env_vars

severity = ["", "INFO", "WARNING", "ERROR"]

def create_sysinfo_message():
        conf = get_env_vars()
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        spec_id = str(uuid.uuid4())
        now = datetime.now().strftime("%x %X")
        channel.basic_publish(exchange=conf["EX"]["SYS_EXCHANGE"], routing_key="", body=f"[{now}][{spec_id}]: this message will broadcast sysinfo msg.")
        print(" [x] Sent demo broadcast into exchange")
        connection.close()


def create_businessInfo_message():
        conf = get_env_vars()
        connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = connection.channel()
        spec_id = str(uuid.uuid4())

        for item in severity:
                now = datetime.now().strftime("%x %X")
                channel.basic_publish(exchange=conf["EX"]["BIZ_EXCHANGE"], routing_key=item, body=f"[{now}][From {spec_id}]: this message will be sent to bizINFO queue.")

        print(" [x] Sent demo message to business info exchange")
        connection.close()


if __name__ == '__main__':
        create_sysinfo_message()
        time.sleep(3)
        create_businessInfo_message()