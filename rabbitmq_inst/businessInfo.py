#! /usr/bin/python3

import pika
import sys
from datetime import datetime
import re
from m_libs import get_env_vars

severity = ["", "INFO", "WARNING", "ERROR"]


def biz_proc_cb(ch, method, properties, body):
	handler = "Topic {0} Handler".format(method.routing_key)
	print("[Consumer: {0}] Received message: {1}".format(handler, body))
	ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
	conf = get_env_vars()
	params = pika.ConnectionParameters("localhost")
	connection = pika.BlockingConnection(parameters=params)
	channel = connection.channel()
	
	for item in severity:
		channel.queue_bind(queue=conf['Q']["BIZ_QUEUE"], exchange=conf["EX"]["BIZ_EXCHANGE"], routing_key=item)
		channel.basic_consume(queue=conf['Q']["BIZ_QUEUE"], on_message_callback=biz_proc_cb, auto_ack=False)
	
	try:
		channel.start_consuming()
	except KeyboardInterrupt:
		print("中斷")
		sys.exit(0)