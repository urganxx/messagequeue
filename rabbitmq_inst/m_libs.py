#! /usr/bin/python3
import configparser
import os


def get_env_vars():
	config = configparser.RawConfigParser()
	exec_path = os.path.dirname(os.path.abspath(__file__))
	conf_file = exec_path + "/rabbitmq_definition.ini"
	try:
		with open(conf_file, 'r') as f:
			config.read_file(f)
			mq_server_vars = {"SYS": dict(), "EX": dict(), "Q": dict()}
			mq_server_vars["SYS"]["USER"] = config.get("rabbitmq", "USER")
			mq_server_vars["SYS"]["AUTH"] = config.get("rabbitmq", "AUTH")
			mq_server_vars["EX"]['SYS_EXCHANGE'] = config.get("exchanges", "SYS_EXCHANGE")
			mq_server_vars["EX"]['BIZ_EXCHANGE'] = config.get("exchanges", "BIZ_EXCHANGE")
			mq_server_vars["EX"]['DLX_EXCHANGE'] = config.get("exchanges", "DLX_EXCHANGE")
			mq_server_vars["Q"]["SYS_QUEUE"] = config.get("queues", "SYS_QUEUE")
			mq_server_vars["Q"]["BIZ_QUEUE"] = config.get("queues", "BIZ_QUEUE")
			mq_server_vars["Q"]["DLQ_QUEUE"] = config.get("queues", "DLQ_QUEUE")
			
			return mq_server_vars
	except OSError:
		exit()
