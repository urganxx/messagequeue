#! /usr/bin/python3

import pika
import sys
from datetime import datetime
from m_libs import get_env_vars


def sysinfo_cb(ch, method, properties, body):
	print(body)
	ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == "__main__":
	conf = get_env_vars()
	params = pika.ConnectionParameters("localhost")
	connection = pika.BlockingConnection(parameters=params)
	channel = connection.channel()
	sys_queue = channel.queue_declare(queue=conf['Q']["SYS_QUEUE"]+"1", durable=True, exclusive=True)
	channel.queue_bind(queue=sys_queue.method.queue, exchange=conf["EX"]["SYS_EXCHANGE"], routing_key="")
	channel.basic_consume(queue=sys_queue.method.queue, on_message_callback=sysinfo_cb, auto_ack=False)
	try:
		channel.start_consuming()
	except KeyboardInterrupt:
		print("中斷")
		sys.exit(0)
