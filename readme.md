# MessageQueue  Project <UberDemo>
## Pre-prepare requirements
1. erlang    ( 23.1.1-1.el7 )
2. rabbitmq-server ( 3.8.9 ) 
	1. add /etc/rabbitmq/rabbitmq.conf
	2. add /etc/rabbitmq/rabbitmq-env.conf
	
3. rabbitmq-plugins enable [required plugins]
	* rabbitmq_mqtt
	* rabbitmq_federation
	* rabbitmq_management
	* amqp_client
	* rabbitmq_management_agent
	
4. add rabbitmq control panel admin/users
5. systemctl restart rabbitmq-server.service

6. pip install peewee 
7. pip install glances

## Azure: All services > Virtual machines >  ResourceGroupName
* Networking > Add inbound port rule:

[snapshot](./doc/fig1.png)


